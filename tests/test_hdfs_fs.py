import csv
import json
import lzma
import os
import pickle
import re
import tarfile
from contextlib import contextmanager
from io import StringIO
from typing import Iterator, Optional
from zipfile import ZipFile

import pexpect
import pytest
from py4j.java_gateway import JavaGateway, GatewayParameters
from py4j.protocol import Py4JJavaError

from jhdfs4py import HdfsFs

_gateway_server_process: Optional[pexpect.spawn] = None
_gateway_server: Optional[JavaGateway] = None


def _stop_test_gateway_server():
    global _gateway_server_process
    global _gateway_server

    if _gateway_server:
        _gateway_server.shutdown()
        _gateway_server = None

    if _gateway_server_process:
        _gateway_server_process.terminate(force=True)
        _gateway_server_process = None


@pytest.fixture(scope="session", autouse=True)
def _start_test_gateway_server(request):
    global _gateway_server

    if os.getenv("USE_EXTERNAL_GATEWAY_SERVER") in {"1", "yes", "true"}:
        _gateway_server = JavaGateway()
        return

    if os.name != "posix":
        raise Exception(
            """Starting the gateway server automatically is only supported on posix systems.
            Please start the server manually as outlined in README.MD and add USE_EXTERNAL_GATEWAY_SERVER=1
            to your environment."""
        )

    global _gateway_server_process
    if not _gateway_server_process:
        path = os.path.dirname(__file__)
        sbt_script = os.getenv("SBT_SCRIPT", "sbt")
        try:
            working_dir = f"{path}/py4j-test-server"
            sbt_run = f"{sbt_script} run"
            env = os.environ.copy()
            env["USE_DYNAMIC_PORTS"] = "1"
            _gateway_server_process = pexpect.spawn(
                command=sbt_run, cwd=working_dir, env=env, encoding="UTF-8"
            )
            listening_re = re.compile("Gateway started and listening on port (\\d+)")
            _gateway_server_process.expect(listening_re, timeout=180)
            listening_line = _gateway_server_process.after
            print(f"Got line '{listening_line}'")
            port_search = re.search(listening_re, listening_line)
            if not port_search:
                raise Exception("Could not extract listening port")
            else:
                server_port = int(port_search.group(1))
                _gateway_server = JavaGateway(
                    gateway_parameters=GatewayParameters(port=server_port)
                )
        except Exception as e:
            raise Exception("Error starting gateway server") from e
        finally:
            request.addfinalizer(_stop_test_gateway_server)


@contextmanager
def make_test_fs() -> Iterator[HdfsFs]:
    global _gateway_server
    assert _gateway_server is not None
    j_filesystem = _gateway_server.jvm.org.apache.hadoop.fs.RawLocalFileSystem
    j_uri = _gateway_server.jvm.java.net.URI
    j_configuration = _gateway_server.jvm.org.apache.hadoop.conf.Configuration
    conf = j_configuration(False)
    root_uri = j_uri.create("file:///")
    fs = j_filesystem()
    fs.initialize(root_uri, conf)

    try:
        yield HdfsFs(_gateway_server, fs)
    finally:
        fs.close()


def test_read_write_strings(tmpdir):
    test_string = "test123"
    test_file_name = "test.txt"
    file = tmpdir.join(test_file_name)

    with make_test_fs() as test_fs:
        test_fs.write_string(file, test_string)
        res = test_fs.read_string(file)
        assert res == test_string


def test_walk(tmpdir):
    with make_test_fs() as test_fs:
        # base
        #  |  \
        #  f1 \
        #     \
        #     d1---------------------
        #    /  \      |     |      |
        #  f11 f12    d12   d13    d14
        #                    |      |
        #                    f131  f141

        base = tmpdir
        f1 = base.join("f1")
        d1 = base.join("d1")
        f11 = d1.join("f11")
        f12 = d1.join("f12")
        d12 = d1.join("d12")
        d13 = d1.join("d13")
        f131 = d13.join("f131")
        d14 = d1.join("d14")
        f141 = d14.join("f141")

        for d in d1, d12, d13, d14:
            test_fs.mkdirs(d)

        test_fs.write_string(f1, "f1")
        test_fs.write_string(f11, "f11")
        test_fs.write_string(f12, "f12")
        test_fs.write_string(f131, "f131")
        test_fs.write_string(f141, "f141")

        test_fs.set_permissions(d14, 0o000)
        try:
            res0 = list(
                (
                    str(b).replace("\\", "/"),
                    dns,
                    [fn for fn in fns if not fn.endswith(".crc")],
                )
                for b, dns, fns in os.walk(base)
            )
            res = list(test_fs.walk(base))
            assert res == res0
        finally:
            test_fs.set_permissions(d14, 0o700)


def test_read_write_bytes(tmpdir):
    test_bytes = b"test123"
    test_file_name = "test.txt"
    f = tmpdir.join(test_file_name)

    with make_test_fs() as test_fs:
        test_fs.write_bytes(f, test_bytes)
        assert test_fs.read_bytes(f) == test_bytes


def test_set_get_permissions(tmpdir):
    test_string = "test123"
    test_file_name = "test.txt"
    test_dir_name = "test"
    folder = tmpdir.join(test_dir_name)
    file = folder.join(test_file_name)

    with make_test_fs() as test_fs:
        test_fs.mkdirs(folder, permissions=0o700)
        assert test_fs.get_permissions(folder) == 0o700
        test_fs.write_string(file, test_string, permissions=0o600)
        assert test_fs.get_permissions(file) == 0o600
        assert test_fs.read_string(file) == test_string


def test_read_write_strings_with_open(tmpdir):
    lines = ["1", "2", "3"]
    lines_with_newlines = [line + "\n" for line in lines]
    test_file_name = "test.txt"
    f = tmpdir.join(test_file_name)

    with make_test_fs() as test_fs:
        with test_fs.open(f, "w") as io:
            io.writelines(lines_with_newlines)

        with test_fs.open(f, "r") as io:
            assert io.read(1) == "1"
            assert io.read(1) == "\n"
            assert io.readlines() == lines[1:]


def test_write_read_csv_using_python_lib(tmpdir):
    with make_test_fs() as test_fs:
        csv_file = tmpdir.join("test.csv")

        with test_fs.open(csv_file, "wt") as csv_io:
            csv_writer = csv.writer(csv_io)
            csv_writer.writerow([11, 12, 13])
            csv_writer.writerow([21, 22, 23])

        with test_fs.open(csv_file, "rt") as csv_io:
            csv_reader = csv.reader(csv_io)
            rows = list(csv_reader)
            assert rows == [["11", "12", "13"], ["21", "22", "23"]]


def test_write_read_json(tmpdir):
    with make_test_fs() as test_fs:
        json_file = tmpdir.join("test.json")
        data = {"name": "Friz", "age": 42}

        with test_fs.open(json_file, "wt") as json_io:
            json.dump(data, json_io)

        with test_fs.open(json_file, "rt") as json_io:
            data2 = json.load(json_io)
            assert data == data2


def test_write_and_read_csv_using_pandas(tmpdir):
    with make_test_fs() as test_fs:
        try:
            import pandas

            csv_file = tmpdir.join("test.csv")

            raw_data = {"name": ["Asterix", "Obelix", "Idefix"], "age": [42, 45, 5]}

            with test_fs.open(csv_file, "wt") as io:
                df = pandas.DataFrame(raw_data, columns=["name", "age"])
                df.to_csv(io)

            with test_fs.open(csv_file, "rt") as io:
                df = pandas.read_csv(io)
                assert len(df) == 3

        except ImportError as e:
            pytest.skip(f"Error loading pandas: {e}")


def test_write_and_read_tar_gz(tmpdir):
    with make_test_fs() as test_fs:
        tar_file = tmpdir.join("test.tar.gz")

        with test_fs.open(tar_file, "wb") as io:
            with tarfile.open(fileobj=io, mode="w|gz") as tar_io:
                tar_io.addfile(tarfile.TarInfo("test.txt"), StringIO("test"))

        with test_fs.open(tar_file, "rb") as io:
            with tarfile.open(fileobj=io, mode="r|gz") as tar_io:
                assert tar_io.getnames() == ["test.txt"]


def test_write_and_read_zip(tmpdir):
    with make_test_fs() as test_fs:
        zip_file = tmpdir.join("test.zip")

        with test_fs.open(zip_file, "wb") as zip_io:
            with ZipFile(zip_io, "w") as zip:
                zip.writestr("test1.txt", "test1")
                zip.writestr("test2.txt", "test2")

        with test_fs.open(zip_file, "rb") as zip_io:
            with ZipFile(zip_io, "r") as zip:
                zip.testzip()
                assert zip.read("test2.txt") == b"test2"
                assert zip.namelist() == ["test1.txt", "test2.txt"]


def test_close_and_closed(tmpdir):
    with make_test_fs() as test_fs:
        test_file = tmpdir.join("test.data")

        text_io = test_fs.open(test_file, "w")
        try:
            assert not text_io.closed
            text_io.close()
        finally:
            text_io.close()
        assert text_io.closed

        bin_io = test_fs.open(test_file, "wb")
        try:
            assert not bin_io.closed
            bin_io.close()
        finally:
            bin_io.close()
        assert bin_io.closed

        with test_fs.open(test_file, "w") as text_io2:
            assert not text_io2.closed
        assert text_io2.closed

        with test_fs.open(test_file, "wb") as bin_io2:
            assert not bin_io2.closed
        assert bin_io2.closed


def test_write_and_read_bytes(tmpdir):
    with make_test_fs() as test_fs:
        test_file = tmpdir.join("test.data")

        with test_fs.open(test_file, "wb") as io:
            io.write("test".encode("utf-8"))

        with test_fs.open(test_file, "rb") as io:
            s = io.readall().decode("utf-8")
            assert s == "test"


def test_create_delete_list(tmpdir):
    test_dir = tmpdir.join("test")
    test_file1 = test_dir.join("file1.bin")
    test_file2 = test_dir.join("file2.bin")

    with make_test_fs() as test_fs:
        test_fs.mkdirs(test_dir)
        test_fs.write_bytes(test_file1, bytes.fromhex("caffeebabe"))
        test_fs.write_bytes(test_file2, bytes.fromhex("deadbeef"))

        assert test_fs.listdir(tmpdir) == ["test"]
        assert len(test_fs.listdir(test_dir)) == 2

        test_fs.delete(test_file1, recursive=False)
        assert len(test_fs.listdir(test_dir)) == 1

        with pytest.raises(Py4JJavaError):
            test_fs.delete(test_dir)
        test_fs.delete(test_dir, recursive=True)

        assert not test_fs.listdir(tmpdir)


def test_create_delete_empty_and_not_empty_dirs(tmpdir):
    test_dir1 = tmpdir.join("test1")
    test_file1 = test_dir1.join("file1.bin")
    test_dir2 = tmpdir.join("test2")

    with make_test_fs() as test_fs:
        test_fs.mkdirs(test_dir1)
        test_fs.mkdirs(test_dir2)

        test_fs.write_string(test_file1, "asdf")

        with pytest.raises(Py4JJavaError):
            test_fs.delete(test_dir1, recursive=False)

        assert test_fs.delete(test_dir1, recursive=True)
        assert test_fs.delete(test_dir2, recursive=False)


def test_append_to_text_file(tmpdir):
    txt_file = tmpdir.join("test.txt")

    with make_test_fs() as test_fs:
        test_fs.write_string(txt_file, "line0\n")

        with test_fs.open(txt_file, "at") as io:
            io.writelines(["line1\n", "line2"])

        text = test_fs.read_string(txt_file)
        assert text == "line0\nline1\nline2"


def test_append_to_bin_file(tmpdir):
    bin_file = tmpdir.join("test.bin")

    with make_test_fs() as test_fs:
        test_fs.write_bytes(bin_file, bytes.fromhex("01"))

        with test_fs.open(bin_file, "ab") as io:
            io.write(bytes.fromhex("02"))

        bs = test_fs.read_bytes(bin_file)
        assert bs == bytes.fromhex("01 02")


def test_open_with_illegal_mode_strings(tmpdir):
    test_file = tmpdir.join("test.xyz")

    with make_test_fs() as test_fs:

        def assert_mode_rejected(mode):
            with pytest.raises(ValueError):
                with test_fs.open(test_file, mode=mode):
                    pass

        assert_mode_rejected("???")
        assert_mode_rejected("rtb")
        assert_mode_rejected("rwb")
        assert_mode_rejected("rrb")
        assert_mode_rejected("wbb")
        assert_mode_rejected("+t")
        assert_mode_rejected("xb")


def test_exists(tmpdir):
    test_file = tmpdir.join("test.abc")

    with make_test_fs() as test_fs:
        assert not test_fs.exists(test_file)
        test_fs.write_string(test_file, "test")
        assert test_fs.exists(test_file)


def test_rename(tmpdir):
    test_file1 = tmpdir.join("test1.txt")
    test_file2 = tmpdir.join("test2.txt")

    with make_test_fs() as test_fs:
        test_fs.write_string(test_file1, "test")
        assert test_fs.rename(test_file1, test_file2)
        assert not test_fs.exists(test_file1)
        assert test_fs.exists(test_file2)
        assert test_fs.rename(test_file2, test_file1)
        assert test_fs.exists(test_file1)
        assert not test_fs.exists(test_file2)


def test_read_write_lzma_pickle(tmpdir):
    test_file = tmpdir.join("test.obj.xz")
    test_obj = {"test1": tuple([0] * 128), "test2": tuple(range(4 * 1024))}

    with make_test_fs() as test_fs:
        with test_fs.open(test_file, mode="wb") as f:
            with lzma.open(
                f, mode="wb", format=lzma.FORMAT_XZ, check=lzma.CHECK_CRC64
            ) as lzma_f:
                pickle.dump(test_obj, lzma_f)

        with test_fs.open(test_file, mode="rb") as f:
            with lzma.open(f, mode="rb", format=lzma.FORMAT_AUTO) as lzma_f:
                loaded_obj = pickle.load(lzma_f)
                assert loaded_obj == test_obj
