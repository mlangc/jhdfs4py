package org.zsvr.py4j.test


import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import py4j.DefaultGatewayServerListener
import py4j.GatewayServer
import py4j.GatewayServer.GatewayServerBuilder

object TestGatewayServer {
  def main(args: Array[String]): Unit = {
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()
    GatewayServer.turnLoggingOff()
    val logger = LoggerFactory.getLogger(getClass)

    val useDynamicPort = Option(System.getenv("USE_DYNAMIC_PORTS")).nonEmpty
    val server = new GatewayServerBuilder()
      .javaPort(if (useDynamicPort) 0 else GatewayServer.DEFAULT_PORT)
      .build()


    server.addListener(new DefaultGatewayServerListener {
      override def serverStarted(): Unit = {
        logger.info(s"Gateway started and listening on port ${server.getListeningPort}")
      }
    })


    server.start()
  }
}
