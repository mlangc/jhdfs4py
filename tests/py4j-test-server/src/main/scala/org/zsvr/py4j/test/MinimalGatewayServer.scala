package org.zsvr.py4j.test

import py4j.GatewayServer

object MinimalGatewayServer {
  def main(args: Array[String]): Unit = {
    val server = new GatewayServer()
    server.start()
  }
}
