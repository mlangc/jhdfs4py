import os
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp
from timeit import timeit

from tests.test_hdfs_fs import make_test_fs


def _create_deeply_nested_dir_tree_and_dummy_files(basedir):
    basedir = Path(basedir)

    n_child_dirs = 3
    n_files_in_leaf_dir = 9
    n_files_in_non_leaf_dir = 5
    depth = 4
    n_leaf_dirs = n_child_dirs ** depth
    n_non_leaf_dirs = sum(n_child_dirs ** i for i in range(1, depth))

    n_files = (
        n_non_leaf_dirs + 1
    ) * n_files_in_non_leaf_dir + n_leaf_dirs * n_files_in_leaf_dir

    n_dirs = n_leaf_dirs + n_non_leaf_dirs
    print(f"About to create {n_dirs} folders and {n_files} files...")

    def create_dir_tree(basedir, depth):
        if depth > 0:
            for i in range(0, n_files_in_non_leaf_dir):
                with open(basedir / f"file_{i}.txt", mode="x") as file:
                    print(i, file=file)

            for i in range(0, n_child_dirs):
                dir = basedir / f"dir_{i}"
                os.mkdir(dir)
                create_dir_tree(dir, depth - 1)
        else:
            for i in range(0, n_files_in_leaf_dir):
                with open(basedir / f"file_{i}.txt", mode="x") as file:
                    print(i, file=file)

    create_dir_tree(basedir, depth)
    print("Done")


if __name__ == "__main__":
    tmpdir = mkdtemp(suffix=".benchmark", prefix="jhdfs4py")
    try:
        _create_deeply_nested_dir_tree_and_dummy_files(tmpdir)
        hdfs = make_test_fs()

        def count_files_and_dirs_by_walking():
            dir_count, file_count = 0, 0

            for root, dirs, files in hdfs.walk(tmpdir):
                dir_count += len(dirs)
                file_count += len(files)

            print(f"Counted {dir_count} directories and {file_count} files")

        runs = 3
        runtime = timeit(count_files_and_dirs_by_walking, number=runs) / runs
        print(f"Runtime: {runtime}s")
    finally:
        rmtree(tmpdir)
