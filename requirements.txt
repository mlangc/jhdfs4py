black
wheel
mypy
py4j<=0.10.7
pytest
pandas
data-science-types
pyspark>=2.4.0,<2.5
pyspark-stubs>=2.4.0,<2.5
dataclasses>=0.7; python_version < '3.7'
types-dataclasses; python_version < '3.7'
pexpect
